<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function show($conteudo, $menu = true) {
        $html = $this->load->view('commom/cabecalho', null, true);
        if($menu) $html .= $this->load->view('commom/navbar', null, true);
        $html .= $conteudo;
        $html .= $this->load->view('commom/rodape', null, true);
        echo $html;
    }

}